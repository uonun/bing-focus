# Wallpapers from Bing Focus

本脚本用于从 Bing 站点获取高清图片，下载到指定位置之后，可设置为电脑桌面。

## 参数介绍

```bash
# -n 下载最近 n 天的图片（默认为 1）
# -d 图片下载存放位置（默认为 ~/Pictures/focus）
./focus.py -n <latest n> -d <output dir path>
```

## 在 MacOS 上自动运行

```xml
<!-- ~/Library/LaunchAgents/bing-focus.plist -->
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
	<dict>
		<key>Label</key>
		<string>bing-focus.agent</string>
		<key>ProgramArguments</key>
		<array>
			<!-- 指定脚本路径 -->
			<string>/PATH/TO/focus.py</string>
			<!-- 参数配置 -->
			<string>-n</string>
			<string>3</string>
		</array>
		<key>RunAtLoad</key>
		<true/>
		<key>StartInterval</key>
		<!-- 每天执行一次 -->
		<integer>86400</integer>
		<key>StandardErrorPath</key>
		<string>/PATH/TO/Logs/bing-focus.log</string>
		<key>StandardOutPath</key>
		<string>/PATH/TO/Logs/bing-focus.log</string>
	</dict>
</plist>
```

参考: https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPSystemStartup/Chapters/CreatingLaunchdJobs.html
