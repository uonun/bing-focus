#!/usr/bin/python3
# -*- coding: utf-8 -*-

from __future__ import print_function
import os
import getopt
import sys
import json
import platform
import datetime
import shutil
import pathlib, fnmatch


try:
    # Python2
    from urlparse import urlparse
    from urlparse import urljoin
    from urlparse import parse_qs
    import urllib as request
except ImportError:
    # Python3
    from urllib.parse import urlparse
    from urllib.parse import urljoin
    from urllib.parse import parse_qs
    import urllib.request as request

BING_FOCUS_URL = "https://cn.bing.com/HPImageArchive.aspx?format=js&idx=0&n=$N&nc=&pid=hp&uhd=1&uhdwidth=3840&uhdheight=2160"
BING_Base = "https://cn.bing.com"
DEFAULT_OUTPUT_DIR = os.path.join(os.path.expanduser('~'), "Pictures", "focus")
DEFAULT_LATEST_N = "1"


def get_items(n):
    "获取最近 N 天的焦点图"
    print("获取图片路径...", end="")
    url = str.replace(BING_FOCUS_URL, "$N", n)
    print(url)
    res = request.urlopen(url)
    if (res.getcode() == 200):
        contents = res.read()
        data = json.loads(contents)
        items = data["images"]
        print("找到", len(items), "张图片.")
        return items
    else:
        return []


def get_images(items, path):
    "获取图片文件"
    results = []
    idx = 0
    latest_name = ""
    for el in items:
        img_src = el["url"]
        copyright = el["copyright"]
        # print(el)
        print("下载图片[", idx, "]:", copyright, end="")
        idx += 1
        url = urljoin(BING_Base, img_src)
        id = get_file_id(url)
        img_name = el["enddate"]+"-"+id
        filename = os.path.join(path, img_name)
        response = request.urlopen(url)
        if (response.getcode() == 200):
            with open(filename, "wb") as f:
                f.write(response.read())  # 将内容写入图片
            print("....", response.headers.get("Content-Length"), "字节.")

            # 复制一份最新的图片，以备设置为当天桌面
            if (latest_name == ""):
                _, file_extension = os.path.splitext(filename)
                latest_name = os.path.join(path, "latest"+file_extension)
                shutil.copy(filename,latest_name)

            # print "file ", filename, " saved."
            results.append(filename)
    return results


def get_file_id(url):
    query = urlparse(url).query
    id = parse_qs(query).get("id")[0]
    return id


def show_help():
    print(__file__+' -n <latest n> -d <output dir path>')
    print(__file__+' --latest=<latest n> --dir=<output dir path>')

def make_archives(path):
    archive_dir = os.path.join(path,"archives/")
    move_dir(path,archive_dir,"202*")

def move_dir(src: str, dst: str, pattern: str = '*'):
    if not os.path.isdir(dst):
        pathlib.Path(dst).mkdir(parents=True, exist_ok=True)
    for f in fnmatch.filter(os.listdir(src), pattern):
        shutil.move(os.path.join(src, f), os.path.join(dst, f))

def main(argv):
    n = DEFAULT_LATEST_N
    path = DEFAULT_OUTPUT_DIR
    try:
        opts, _ = getopt.getopt(argv, "hn:d:", ["latest=", "dir="])
    except getopt.GetoptError:
        print('unknown argument. please see help:')
        show_help()
        sys.exit(2)
    for opt, arg in opts:
        if opt == "-h":
            show_help()
            sys.exit()
        elif opt in ("-n", "--latest"):
            n = arg
        elif opt in ("-d", "--dir"):
            path = arg

    print("===== WALLPAPERS FROM BING FOCUS =====")
    print(datetime.datetime.now(), "python_version: ", platform.python_version())
    print("获取最近", n, "天的聚焦图，输出到路径：",  os.path.expandvars(path))

    if (not os.path.exists(path)):
        os.mkdir(path)

    data = get_items(n)
    files = get_images(data, path)
    print("图片下载完成，共下载", len(files), "张图片。")
    print("\n")

    make_archives(path)

if __name__ == "__main__":
    main(sys.argv[1:])
